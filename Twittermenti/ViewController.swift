import UIKit
import SwifteriOS
import CoreML
import SwiftyJSON

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var sentimentLabel: UILabel!
    
    let sentimentClassifier = TweetSentimentClassifier()
    
    let swifter = Swifter(consumerKey: "JOqq7oxEUm25fhjXfslxz5RFA",
                          consumerSecret:"QVoFxn8GwtiEKGp26y9g4Yr4vT172zP2fAW3iab25oZHuWvjQD")
    
    let tweetCount = 100
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textField.delegate = self
    }
    
    @IBAction func predictPressed(_ sender: Any) {
        fetchTweets()
    }
    
    func fetchTweets() {
        
        if let searchText = textField.text {
            
            swifter.searchTweet(using: searchText, lang: "en", count: tweetCount, tweetMode: .extended,  success: { (results, metadata) in
                
                var tweets = [TweetSentimentClassifierInput]()
                
                for i in 0..<self.tweetCount {
                    if let tweet = results[i]["full_text"].string {
                        let tweetForClassification = TweetSentimentClassifierInput(text: tweet)
                        tweets.append(tweetForClassification)
                    }
                }
                
                self.makePredictions(with: tweets)
                
                
                
            }) { (error) in
                print("There was an error with the Twitter API request, \(error)")
            }
        }
    }
    
    func makePredictions(with tweets: [TweetSentimentClassifierInput]) {
        
        do {
            let predictions = try self.sentimentClassifier.predictions(inputs: tweets)
            var sentimentScore = 0
            for i in 0...predictions.count - 1 {
                let sentiment = predictions[i].label
                if sentiment == "Pos" {
                    sentimentScore += 1
                }
                else if sentiment == "Neg" {
                    sentimentScore -= 1
                }
                
                print("TWEET: \(tweets[i].text) PREDICTED AS: \(sentiment)")
            }
            

            updateUI(with: sentimentScore)
            
        }
        catch {
            print("There was an error making the prediction, \(error)")
        }
        
    }
    
    func updateUI(with sentimentScore: Int) {
        if sentimentScore > 20 {
            self.sentimentLabel.text = "😍"
        }
        else if sentimentScore > 10 {
            self.sentimentLabel.text = "😀"
        }
        else if sentimentScore == 0 {
            self.sentimentLabel.text = "😐"
        }
        else if sentimentScore > -10 {
            self.sentimentLabel.text = "🙁"
        }
        else if sentimentScore > -20 {
            self.sentimentLabel.text = "🤬"
        }
        else  {
            self.sentimentLabel.text = "🤮"
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    
}

