Twitter Sentiment analyzer is an app developed for a challenge at the Apple Developer Academy that uses a machine learning model trained by to understand 
if opinions presented in tweets is positive or negative. It  then calculates how many
opinions were positive or negative to present the overall opinion of the twitter userbase on the topic.

For more information check out my medium article: 

https://medium.com/apple-developer-academy-federico-ii/twitter-sentiment-analyser-2f8852ed98e5